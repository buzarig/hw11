"use strict";

const input = document.querySelectorAll(".input");
const inputArray = [...input];

const icons = document.querySelectorAll(".icon-password");
const iconsArray = [...icons];

const submitButton = document.querySelector(".btn");

function showPass(el) {
  el.classList.remove("fa-eye-slash");
  el.classList.add("fa-eye");
}

function hidePass(el) {
  el.classList.remove("fa-eye");
  el.classList.add("fa-eye-slash");
}

iconsArray.forEach((icon, index) => {
  icon.addEventListener("click", () => {
    if (icon.classList.contains("fa-eye-slash")) {
      showPass(icon);
      inputArray[index].setAttribute("type", "text");
    } else {
      hidePass(icon);
      inputArray[index].setAttribute("type", "password");
    }
  });
});

submitButton.addEventListener("click", (event) => {
  event.preventDefault();
  console.log(inputArray[0].value.length);
  if (
    inputArray[0].value === inputArray[1].value &&
    inputArray[0].value.length > 0 &&
    inputArray[1].value.length > 0
  ) {
    alert("You are welcome");
  } else if (inputArray[0].value !== inputArray[1].value) {
    alert("Потрібно ввести однакові значення");
  }
});
